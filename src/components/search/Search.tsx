import React, { useState } from "react";
import { collection, getDocs, query, where } from "firebase/firestore";
import { Button, Form } from "react-bootstrap";
import db from "../../database/config";

const Search = () => {
  const [data, setdata] = useState<Array<any>>();
  const [textFilter, setTextFilter] = useState("");

  const handleGetData = async () => {
    let queryBody;
    try {
      const citiesTransmisionRef = collection(db, "permisos");

      if (textFilter !== "") {
        queryBody = query(citiesTransmisionRef, where("titulo", "==", textFilter)
        );
      } else {
        queryBody = query(citiesTransmisionRef);
      }
      const response = await getDocs(queryBody);
      console.log(response.docs)
      const dataTransmission: any = [];
      response.docs.forEach((data) => {
        dataTransmission.push(data.data());
      });
      setdata(dataTransmission);
      console.log(data)

    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Titulo de la Predica</Form.Label>
        <Form.Control
          type="text"
          placeholder="La palabra de Dios...."
          onChange={(e) => setTextFilter(e.target.value)}
        />
        <Form.Text className="text-muted">Debe ser exacto.</Form.Text>
      </Form.Group>
      <Button variant="primary" onClick={handleGetData}>
        Buscar
      </Button>
    </Form>
  );
};

export default Search;
